﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorSpecific : MonoBehaviour
{
    private SpriteRenderer m_SpriteRenderer;
    private bool isColored;

    void Start()
    {
        m_SpriteRenderer = GetComponent<SpriteRenderer>();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (!isColored)
        {
            if (col.gameObject.tag == "red crayon")
            {
                if (this.gameObject.tag == "red only")
                {
                    m_SpriteRenderer.color = new Color(1, 0, 0, 1);
                    isColored = true;
                }
            }
            else if (col.gameObject.tag == "green crayon")
            {
                if (this.gameObject.tag == "green only")
                {
                    m_SpriteRenderer.color = new Color(0, 1, 0, 1);
                    isColored = true;
                }
            }
            else if (col.gameObject.tag == "orange crayon")
            {
                if (this.gameObject.tag == "orange only")
                {
                    m_SpriteRenderer.color = new Color(1, 0.75f, 0, 1);
                    isColored = true;
                }
            }
            else if (col.gameObject.tag == "yellow crayon")
            {
                if (this.gameObject.tag == "yellow only")
                {
                    m_SpriteRenderer.color = new Color(1, 1, 0, 1);
                    isColored = true;
                }
            }
            else if (col.gameObject.tag == "blue crayon")
            {
                if (this.gameObject.tag == "blue only")
                {
                    m_SpriteRenderer.color = new Color(0, 0, 1, 1);
                    isColored = true;
                }
            }
            else if (col.gameObject.tag == "purple crayon")
            {
                if (this.gameObject.tag == "purple only")
                {
                    m_SpriteRenderer.color = new Color(0.5f, 0, 1, 1);
                    isColored = true;
                }
            }
            else if (col.gameObject.tag == "black crayon")
            {
                m_SpriteRenderer.color = new Color(0, 0, 0, 1);
            }
        }
    }
}
