﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    private Scene scene;

    public GameObject endScreen;
    public Text endText;

    public GameObject player;

    public static int cscore;

    void Start()
    {   cscore = Colorable.ColorCount + manypanel.ColorCountPanel + manypanel2.ColorCountPanel + manypanel3.ColorCountPanel + manypanel4.ColorCountPanel;
        scene = SceneManager.GetActiveScene();
        player = GameObject.Find("Player");
    }

   

    //door opener
    public void BedroomDoor()
    {
        if (scene.name == "Bedroom")
        {
            SceneManager.LoadScene("Kitchen");
        }
        else
        {
            SceneManager.LoadScene("Bedroom");
        }
    }

    public void EndGame()
    {
        endScreen.SetActive(true);
        if (cscore >= 10)
        {
            endText.text = "Talking with your loved ones has really helped you see how bright and colorful the world can be. You feel like you’ve truly made a fresh start.";
        }
        else if (cscore > 5)
        {
            endText.text = "You made a lot of progress today! But you feel like there’s still more to be said to get your fresh start.";
        }
        else if (cscore <= 0)
        {
            endText.text = "You didn’t talk much today. You still feel conflicted. Perhaps one day you'll get the fresh start you’re looking for.";
        }
    }

    public void Restart()
    {
        Destroy(player);
        SceneManager.LoadScene("Bedroom");
        Colorable.ColorCount = 0;
    }
}
