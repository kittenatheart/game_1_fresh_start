﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
	Rigidbody2D rB2D;
	public float runSpeed;

	void Start()
	{
		rB2D = GetComponent<Rigidbody2D>();
	}

	void Update()
	{
		float horizontalInput = Input.GetAxis("Horizontal");
		float verticalInput = Input.GetAxis("Vertical");
		rB2D.velocity = new Vector2(horizontalInput * runSpeed * Time.deltaTime, verticalInput * runSpeed * Time.deltaTime);
	}	
}
