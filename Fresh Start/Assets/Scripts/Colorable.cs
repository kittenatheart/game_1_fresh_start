﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Colorable : MonoBehaviour
{
    SpriteRenderer m_SpriteRenderer;
    private bool isColored;
    public static int ColorCount;

    void Start()
    {
        m_SpriteRenderer = GetComponent<SpriteRenderer>();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (!isColored)
        {
            if (col.gameObject.tag == "red crayon")
            {
                m_SpriteRenderer.color = new Color(1, 0, 0, 1);
                isColored = true;
                ColorCount++;
            }
            else if (col.gameObject.tag == "green crayon")
            {
                m_SpriteRenderer.color = new Color(0, 1, 0, 1);
                isColored = true;
                ColorCount++;
            }
            else if (col.gameObject.tag == "orange crayon")
            {
                m_SpriteRenderer.color = new Color(1, 0.75f, 0, 1);
                isColored = true;
               ColorCount++;
            }
            else if (col.gameObject.tag == "yellow crayon")
            {
                m_SpriteRenderer.color = new Color(1, 1, 0, 1);
                isColored = true;
                ColorCount++;
            }
            else if (col.gameObject.tag == "blue crayon")
            {
                m_SpriteRenderer.color = new Color(0, 0, 1, 1);
                isColored = true;
                ColorCount++;
            }
            else if (col.gameObject.tag == "purple crayon")
            {
                m_SpriteRenderer.color = new Color(0.5f, 0, 1, 1);
                isColored = true;
                ColorCount++;
            }
            else if (col.gameObject.tag == "black crayon")
            {
                m_SpriteRenderer.color = new Color(0, 0, 0, 1);
                isColored = true;
                ColorCount++;
            }
        }
    }
}
