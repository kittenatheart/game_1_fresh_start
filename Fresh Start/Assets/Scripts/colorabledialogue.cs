﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class colorabledialogue : MonoBehaviour
{
    SpriteRenderer m_SpriteRenderer;

    private bool IsColored = false;

    public GameObject Text2;
    public GameObject Text1;

    public int Score;

    void Start()
    {
        m_SpriteRenderer = GetComponent<SpriteRenderer>();
        Text2.SetActive(false);

    }

    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (true)
        {
            if (col.gameObject.tag == "red crayon")
            {
                Image img = GameObject.Find("Text1").GetComponent<Image>();
                img.color = new Color(1, 0, 0, 1);
                m_SpriteRenderer.color = new Color(1, 0, 0, 1);
                IsColored = true;
            }
            else if (col.gameObject.tag == "green crayon")
            {
                Image img = GameObject.Find("Text1").GetComponent<Image>();
                img.color = new Color(0, 1, 0, 1);
                m_SpriteRenderer.color = new Color(0, 1, 0, 1);
                IsColored = true;
            }
            else if (col.gameObject.tag == "orange crayon")
            {
                Image img = GameObject.Find("Text1").GetComponent<Image>();
                img.color = new Color(1, 0.75f, 0, 1);
                m_SpriteRenderer.color = new Color(1, 0.75f, 0, 1);
                IsColored = true;
            }
            else if (col.gameObject.tag == "yellow crayon")
            {
                Image img = GameObject.Find("Text1").GetComponent<Image>();
                img.color = new Color(1, 1, 0, 1);
                m_SpriteRenderer.color = new Color(1, 1, 0, 1);
                IsColored = true;
            }
            else if (col.gameObject.tag == "blue crayon")
            {
                Image img = GameObject.Find("Text1").GetComponent<Image>();
                img.color = new Color(0, 0, 1, 1);
                m_SpriteRenderer.color = new Color(0, 0, 1, 1);
                IsColored = true;
            }
            else if (col.gameObject.tag == "purple crayon")
            {
                Image img = GameObject.Find("Text1").GetComponent<Image>();
                img.color = new Color(0.5f, 0, 1, 1);
                m_SpriteRenderer.color = new Color(0.5f, 0, 1, 1);
                IsColored = true;
            }
            else if (col.gameObject.tag == "black crayon")
            {
                Image img = GameObject.Find("Text1").GetComponent<Image>();
                img.color = new Color(0, 0, 0, 1);
                m_SpriteRenderer.color = new Color(0, 0, 0, 1);
                IsColored = true;
            }

            if (IsColored == true) ;
            {

                Score++;

                if (Score >= 2)
                {
                    Debug.Log("time to switch panel");


                    Text1.SetActive(false);
                    Text2.SetActive(true);

                }
                //Text2.gameObject.SetActive(true);
                // Text1.gameObject.SetActive(false);

            }



        }
    }
}
