﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class colorabledialogue2 : MonoBehaviour
{
    SpriteRenderer m_SpriteRenderer;
    public int colorTime;
    private IEnumerator timer;


    //The Color to be assigned to the Renderer’s Material
    Color m_NewColor;

    //These are the values that the Color Sliders return
    float m_Red, m_Blue, m_Green;

    private bool IsColored2 = false;

    public GameObject Text4;
    public GameObject Text3;

    public int Score2;

    void Start()
    {
        m_SpriteRenderer = GetComponent<SpriteRenderer>();
        //timer = ColorTimer();
        Text4.SetActive(false);

    }

    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (true)
        {
            if (col.gameObject.tag == "red crayon")
            {
                //Debug.Log("redyes");
                // m_SpriteRenderer.color = Color.red;
                Image img = GameObject.Find("Text3").GetComponent<Image>();
                img.color = new Color(1, 0, 0, 1);
                m_SpriteRenderer.color = new Color(1, 0, 0, 1);
                IsColored2 = true;

            }
            else if (col.gameObject.tag == "green crayon")
            {
                //Debug.Log("greenyes");
                //m_SpriteRenderer.color = Color.green;
                Image img = GameObject.Find("Text3").GetComponent<Image>();
                img.color = new Color(0, 1, 0, 1);
                m_SpriteRenderer.color = new Color(0, 1, 0, 1);
                IsColored2 = true;
            }
            else if (col.gameObject.tag == "orange crayon")
            {
                Image img = GameObject.Find("Text3").GetComponent<Image>();
                img.color = new Color(1, 0.75f, 0, 1);
                m_SpriteRenderer.color = new Color(1, 0.75f, 0, 1);
                IsColored2 = true;
            }
            else if (col.gameObject.tag == "yellow crayon")
            {
                Image img = GameObject.Find("Text3").GetComponent<Image>();
                img.color = new Color(1, 1, 0, 1);
                m_SpriteRenderer.color = new Color(1, 1, 0, 1);
                IsColored2 = true;
            }
            else if (col.gameObject.tag == "blue crayon")
            {
                Image img = GameObject.Find("Text3").GetComponent<Image>();
                img.color = new Color(0, 0, 1, 1);
                m_SpriteRenderer.color = new Color(0, 0, 1, 1);
                IsColored2 = true;
            }
            else if (col.gameObject.tag == "purple crayon")
            {
                Image img = GameObject.Find("Text3").GetComponent<Image>();
                img.color = new Color(0.5f, 0, 1, 1);
                m_SpriteRenderer.color = new Color(0.5f, 0, 1, 1);
                IsColored2 = true;
            }
            else if (col.gameObject.tag == "black crayon")
            {
                Image img = GameObject.Find("Text3").GetComponent<Image>();
                img.color = new Color(0, 0, 0, 1);
                m_SpriteRenderer.color = new Color(0, 0, 0, 1);
                IsColored2 = true;
            }

            if (IsColored2 == true) ;
            {

                Score2++;

                if (Score2 >= 2)
                {
                    Debug.Log("time to switch panel");


                    Text3.SetActive(false);
                    Text4.SetActive(true);

                }
                //Text2.gameObject.SetActive(true);
                // Text1.gameObject.SetActive(false);

            }



        }
    }
}
