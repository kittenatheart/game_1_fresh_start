﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Green : MonoBehaviour
{
    private bool isDragging;
    private Vector3 homePosition;

    public PlayerController player;

    private Vector3 lastPosition;
    private float distanceX;
    private float distanceY;

    void Start()
    {
        homePosition = new Vector3(transform.position.x, transform.position.y, transform.position.z);

        player = FindObjectOfType<PlayerController>();

        lastPosition = player.transform.position;
    }

    public void OnMouseDown()
    {
        homePosition = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        isDragging = true;
        gameObject.tag = "green crayon";
    }

    public void OnMouseUp()
    {
        isDragging = false;
        gameObject.tag = "Untagged";
        transform.position = (homePosition);
    }

    void Update()
    {
        if (isDragging)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, 0f);
            Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
            transform.Translate(mousePosition);
        }
        else
        {
            distanceX = player.transform.position.x - lastPosition.x;
            distanceY = player.transform.position.y - lastPosition.y;

            transform.position = new Vector3(transform.position.x + distanceX, transform.position.y + distanceY, transform.position.z);

            lastPosition = player.transform.position;
        }
    }
}
